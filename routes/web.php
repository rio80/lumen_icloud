<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => '/', 'middleware' => 'BasicAuth'], function ($router) {
    $router->post('list_ceklist_template', 'template@list_all_checklists_templates');
    $router->post('create_ceklist_template', 'template@create_checklist_template');
});

// $router->post('list_ceklist_template', 'template@list_all_checklists_templates');
// $router->post('create_ceklist_template', 'template@create_checklist_template');

$router->get('/', function () use ($router) {
    return $router->app->version();
});
