<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Template extends BaseController
{
    function list_all_checklists_templates(Request $req){

        $filter = $req->input('filter');
        $sort = $req->input('sort');
        $fields = $req->input('fields');
        $page_limit = $req->input('page_limit');
        $page_offset = $req->input('page_offset');

        if(!empty($page_limit) && !empty($page_offset)){

            $meta = array(
                "count" => 10,
                "total" => 100,
            );

            $links = array(
                "first" => "https://kong.command-api.kw.com/api/v1/checklists/templates?page[limit]=10&page[offset]=0",
                "last"=> "https://kong.command-api.kw.com/api/v1/checklists/templates?page[limit]=10&page[offset]=10",
                "next"=> "https://kong.command-api.kw.com/api/v1/checklists/templates?page[limit]=10&page[offset]=10",
                "prev"=> "null"
            );

            $checklist = array(
                "description" => "my checklist",
                "due_interval" => 3,
                "due_unit" => "hour"
            );

            $items = array(
                array(
                    "description" => "my foo item",
                    "urgency"=> 2,
                    "due_interval"=> 40,
                    "due_unit"=> "minute"
                ),
                array(
                    "description"=> "my bar item",
                    "urgency"=> 3,
                    "due_interval"=> 30,
                    "due_unit"=> "minute"
                )
                );

            $data = array(
                "name" => "foo template",
                "checklist" => $checklist,
                "items" => $items,
            );

            return response()->json(['meta' => $meta, 'links' => $links , 'data' => $data]);
        }
    }

    function create_checklist_template(Request $req){

        $data = "";
        $attributes = "";

        if($req->isJson()){
            $data = $req->json()->get('data');
            $attributes = $data['attributes'];
        }

        $data_new = array("id" => 1,"attributes" => $attributes);
        return response()->json(['data' => $data_new]);
    }
}
