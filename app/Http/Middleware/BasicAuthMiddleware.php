<?php

  namespace App\Http\Middleware;

  use Closure;

  class BasicAuthMiddleware
  {
      /**
        * Handle an incoming request.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  \Closure  $next
        * @return mixed
        */
      public function handle($request, Closure $next) {
          if($request->getUser() != 'admin' || $request->getPassword() != 'PasswordHere!') {
              $headers = array('WWW-Authenticate' => 'Basic');
              return response('Mohon Masukan Username dan password Basic auth dengan benar', 401, $headers);
          }
          return $next($request);
      }
  }