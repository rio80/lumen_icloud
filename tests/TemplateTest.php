<?php

class TemplateTest extends TestCase
{
    public function test_list_all_checklists_templates()
    {
        $parameters = [
            'page_limit' => 2,
            'page_offset' => 10
        ];
        $this->post("list_ceklist_template", $parameters, ['PHP_AUTH_USER' => 'admin', 'PHP_AUTH_PW' => 'PasswordHere!']);

        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                'meta' =>
                [
                    'count',
                    'total'
                ],
                'links' =>
                [
                    'first',
                    'last',
                    'next',
                    'prev'
                ],
                'data' =>
                [
                    'name',
                    'checklist' => [
                        'description',
                        'due_interval',
                        'due_unit'
                    ],
                    'items'
                ]
            ]
        );
    }

    public function test_create_checklist_template()
    {
        $request = '{
            "data": {
              "attributes": {
                "name": "foo template",
                "checklist": {
                  "description": "my checklist",
                  "due_interval": 3,
                  "due_unit": "hour"
                },
                "items": [
                  {
                    "description": "my foo item",
                    "urgency": 2,
                    "due_interval": 40,
                    "due_unit": "minute"
                  },
                  {
                    "description": "my bar item",
                    "urgency": 3,
                    "due_interval": 30,
                    "due_unit": "minute"
                  }
                ]
              }
            }
          }';

        $send_headers = ['CONTENT_TYPE' => 'application/json', 'PHP_AUTH_USER' => 'admin', 'PHP_AUTH_PW' => 'PasswordHere!'];

        $this->call(
            'POST',
            'create_ceklist_template',
            [],
            [],
            [],
            $send_headers,
            $request
        );

        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                'data' =>
                [
                    'id',
                    'attributes' => [
                        'name',
                        'checklist' => [
                            'description',
                            'due_interval',
                            'due_unit'
                        ],
                        'items'
                    ]
                ],
               
            ]
        );
    }
}
